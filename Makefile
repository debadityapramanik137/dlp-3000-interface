lcr_app: main.c lcr_cmd.c lcr_packetizer.c tcp_client.c
	gcc -Wall $^ -o $@

clean:
	rm *.o lcr_app
