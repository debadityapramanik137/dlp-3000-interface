import cv2
import numpy as np
import time

class dlp():
    def __init__(self):
        self.resolutionx = 684
        self.resolutiony = 608
        self.binary_refresh_rate = 4000
        self.refresh_rate = 120
        self.window = cv2.namedWindow("window",cv2.WND_PROP_FULLSCREEN)
        cv2.setWindowProperty('window',cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)

    def display_binary(self,image):
        cv2.imshow('window',image)
        cv2.waitKey(int(1./self.binary_refresh_rate))

    def display_8bit(self,image):
        cv2.imshow('window',image)
        cv2.waitKey(int(1./self.refresh_rate))

    def display_numpy_binary(self,np_array):
        self.display_binary(np_array)

    def display_numpy_8bit(self,np_array):
        np_array = np_array/255
        self.display_8bit(np_array)

class dlp1():
    def __init__(self):
        self.resolutionx = 64  #can be changed to meet the resolution of the display
        self.resolutiony = 64
        self.binary_refresh_rate = 4000
        self.refresh_rate = 120
        self.window = cv2.namedWindow("window",cv2.WND_PROP_FULLSCREEN)
        cv2.setWindowProperty('window',cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)

    def display_binary(self,image):
        cv2.imshow('window',image)
        cv2.waitKey(int(1./self.binary_refresh_rate))

    def display_8bit(self,image):
        cv2.imshow('window',image)
        cv2.waitKey(int(1./self.refresh_rate))

    def display_numpy_binary(self,np_array):
        self.display_binary(np_array)

    def display_numpy_8bit(self,np_array):
        np_array = np_array
        self.display_8bit(np_array)

if __name__=="__main__":
    projector = dlp()
    dmd_size = (projector.resolutionx,projector.resolutiony)
    projector.display_numpy_8bit(np.random.randint(255,size=dmd_size))
