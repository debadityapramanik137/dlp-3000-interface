import cv2
import numpy as np
import time
import numpy

#Testing it on a full array
#img = np.ones((2160, 3840))

img = np.zeros((2160,3840))
#Testing it on a partially full array
#for i in range(1000):
#    for j in range(2000):
#        img[i][j] = 1

#testing it on a checkerboard array
for i in range(1,2160,5):
    for j in range(0,3840,5):
        img[i][j] = 1
        
#testing on a random array
#for i in range(2160):
#    for j in range(3840):
#        img[i][j] = np.random.randint(2, dtype = int)

imgzero = np.zeros((2160,3840))
cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)
cv2.setWindowProperty("window", cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
while True:
    cv2.imshow("window",img)
    cv2.waitKey(500)
    cv2.imshow("window",imgzero)
    cv2.waitKey(500)